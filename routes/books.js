var model = require("../models/index");

module.exports = function(app) {
  /* GET book all. */
  app.get("/books", function(req, res, next) {
    model.books.findAll({})
      .then(books =>
        res.json({
          error: false,
          data: books
        })
      )
      .catch(error =>
        res.json({
          error: true,
          data: [],
          error: error
        })
      );
  });

/* GET book listing. */
app.get("/books/:id", function(req, res, next) {
    const book_id = req.params.id;
    model.books.findAll(
        {
            where: {
                id: book_id,
            }
        }
        )
        .then(book =>
        res.json({
            error: false,
            data: book
        })
    )   
    .catch(error =>
    res.json({
        error: true,
        data: [],
        error: error
        })
    );
});


  /* POST books. */
  app.post("/books", function(req, res, next) {
    const { title, author, published_date, pages, language, publisher_id } = req.body;
    model.books.create({
        title: title,
        author: author,
        published_date: published_date,
        pages: pages,
        language: language,
        publisher_id: publisher_id
    })
      .then(books =>
        res.status(201).json({
          error: false,
          data: books,
          message: "New book has been created."
        })
      )
      .catch(error =>
        res.json({
          error: true,
          data: [],
          error: error
        })
      );
  });

  /* update books. */
  app.put("/books/:id", function(req, res, next) {
    const books_id = req.params.id;

    const { title, author, published_date, pages, language, publisher_id } = req.body;

    model.books.update(
      {
        title: title,
        author: author,
        published_date: published_date,
        pages: pages,
        language: language,
        publisher_id: publisher_id
      },
      {
        where: {
          id: books_id
        }
      }
    )
      .then(books =>
        res.json({
          error: false,
          message: "book has been updated."
        })
      )
      .catch(error =>
        res.json({
          error: true,
          error: error
        })
      );
  });

  /* GET books listing. */
  /* Delete books. */
  app.delete("/books/:id", function(req, res, next) {
    const books_id = req.params.id;

    model.books.destroy({
      where: {
        id: books_id
      }
    })
      .then(status =>
        res.json({
          error: false,
          message: "book has been delete."
        })
      )
      .catch(error =>
        res.json({
          error: true,
          error: error
        })
      );
  });
};
